const API_URL = 'http://localhost:8080/mercado/rs/fabricantes';

var cadastroFabricante = new Vue({
	el:"#cadastroFabricante",
    data: {
        fabricante : {
        	nome : ''
        },
    },
    methods:{
        salvarFabricante : function(){
			axios.post(`${API_URL}/novo`, this.fabricante)
			.then(() => {
				this.fabricante = {};
				alert('Fabricante cadastrado com sucesso!');
			}).catch(function (error) {
				alert("Error ao cadastrar fabricante");
			}).finally(function() {
			});
		}
    }
});