const API_URL = 'http://localhost:8080/mercado/rs/produtos';

var inicio = new Vue({
	el:"#inicio",
    data: {
    	produto : {
    		id : '',
    		nome : '',
    		volume : '',
    		unidade : '',
    		estoque : '',
    		fabricante : {}
    	},
    	msg : '',
    	listaFabricantes: [],
        listaProdutos: []
    },
    mounted: function(){
        this.buscaProdutos();
        this.buscaFabricantes();
    },
    methods:{
    	 salvarProduto : function(){
    		 if(!this.produto.id){
	    		 axios.post(`${API_URL}/novo`, this.produto)
	 			.then(response => {
	 				alert("Produto cadastrado com sucesso!");
	 				this.buscaProdutos();
	 				this.produto = {};
	 			}).catch(function (error) {
	 				alert("Error ao cadastrar produto");
	 			}).finally(function() {
 				});
	    	 }else{
	    		 axios.put(`${API_URL}/atualizar`, this.produto)
	 			.then((response) => {
	 				alert("Produto atualizado com sucesso!");
	 				this.produto = {};
	 				this.buscaProdutos();
	 			}).catch(function (error) {
	 				alert("Error ao excluir produtos");
	 			});
	    	}
 		},
    	
        buscaProdutos: function(){
			axios.get(`${API_URL}/listar`)
			.then(response => {
				this.listaProdutos = response.data;
			}).catch(function (error) {
				alert("Error ao listar produtos");
			}).finally(function() {
			});
		},
		
		buscaFabricantes: function(){
			const vm = this;
			axios.get(`http://localhost:8080/mercado/rs/fabricantes/lista`)
			.then(response => {
				this.listaFabricantes = response.data;
			}).catch(function (error) {
				alert("Error ao listar fabricantes");
			}).finally(function() {
			});
		},
		
		removerProduto : function(produto){
			if(confirm('Deseja realmente excluir o produto!')){
			axios.delete(`${API_URL}/${produto.id}`)
			.then(() => {
				alert("Produto excluído com sucesso!");
				this.buscaProdutos();
			}).catch(function (error) {
				alert("Error ao excluir produtos");
			});
		  }
		},
		
		atualizarProduto : function(produto){
			this.produto = produto;
		}
    }
});